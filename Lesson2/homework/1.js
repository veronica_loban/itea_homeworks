
let buttons = document.querySelectorAll(".showButton"); //1 бонус done выбрать каждую кнопку одним селектором
let tabContainer = document.getElementById("tabContainer");
let tabs = document.querySelectorAll(".tab");
console.log(tabs);
function hideAllTabs(){
    tabs.forEach((item) => {
        item.classList.remove("active");
    })
}

buttons.forEach( function( button ) {

    let i = button.dataset.tab;
    let k = i - 1;

    //Clearing All images


    button.onclick = function () {
        hideAllTabs();

        tabs[k].classList.add("active"); //0
    }

    })



  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором    done!!

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
