let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
let url_2 = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';

const ConvertToJSON = ( data ) => data.json();

fetch( url )
    .then(ConvertToJSON)
    .then( response1 => {
        //console.log( 'json', response1 );
        let randomPerson = response1[getRandomIntInclusive( 0, 8 )];
        //console.log( randomPerson );
        return fetch( url_2 )
            .then( ConvertToJSON )
            .then( response2 => {
                //console.log(response2);
                let person = new Person( randomPerson.name, randomPerson.phone, response2[0].friends );
                console.log( person )
            })
    })

class Person{
    constructor(name, phone, friends) {
        this.name = name;
        this.phone = phone;
        this.friends = friends;
    }
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random()*(max-min+1)) + min;
}

/*
  Задача:

  1. При помощи fetch получить данные:
    http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName, + age etc
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
