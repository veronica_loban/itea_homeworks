document.addEventListener('DOMContentLoaded', () => {

``
    let form = document.getElementById('json_form');

    form.addEventListener('submit', ( e ) => {
        e.preventDefault();
        let arrayInput = Array.from(form.elements);
        let resObj = {};
        arrayInput.forEach( function(item) {
            if( item.type === 'text'){
                let text = item.value;
                let name = item.name;
                resObj[name] = text;
            }

        });
        let toJson = JSON.stringify(resObj);
        console.log(toJson);
    })

    let input = document.getElementById('json1');
    let button = document.getElementById("send");

    button.addEventListener('click', (e) => {
        var txt = input.value;
        var obj = JSON.parse(txt);
        console.log(obj)
    })
})








/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Array

*/
