/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      !!!+ бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/


var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
var currentPosition = 0;
var slider = document.getElementById('slider');
var PrevSlide = document.getElementById('PrevSilde');
var NextSlide = document.getElementById('NextSilde');
console.log(slider);

OurSliderImages.forEach( function (item) {
    let imageCat = new Image(100, 100);
    imageCat.src = item;
    imageCat.classList.add('loadedImage');
    slider.appendChild(imageCat);
})

let sliderImages = document.querySelectorAll('.loadedImage');
console.log(sliderImages);
//
function reset() {
    for(let i = 0; i < sliderImages.length; i++){
        sliderImages[i].style.display = 'none';
    }
}

window.addEventListener('load', function firstSlide() {
    reset();
    sliderImages[0].style.display = 'block';
})


function slideLeft(){
    reset();
    sliderImages[currentPosition - 1].style.display = 'block';
    currentPosition--;
}

//show next
function slideRight(){
    reset();
    sliderImages[currentPosition + 1].style.display = 'block';
    currentPosition++;
}

PrevSlide.addEventListener('click', function(){
    if(currentPosition === 0){
        currentPosition = sliderImages.length;
    }
    slideLeft();
});

//Right arrow click
NextSlide.addEventListener('click', function(){
    if(currentPosition === sliderImages.length - 1){
        currentPosition = -1;
    }
    slideRight();
});




//window.addEventListener('load', function firstSlide() {

//}
    /*var ImageCat = new Image(100, 100);

    ImageCat.onload = function () {
        ImageCat.classList.add('loadedImage');
    }

    ImageCat.onerror = function (err) {
        new Error('Some Error on load', err);
    };

    ImageCat.src = OurSliderImages[0];
    slider.appendChild(ImageCat);

})

let RenderImage = () => { //( imageCat )
    slider.innerHTML = ''; //очищается наш слайдер
    let imageCat = new Image(100, 100);

    imageCat.onload = function() {
        imageCat.classList.add('loadedImage');
    }    //создаётся наша картинка

    slider.appendChild( imageCat ); // с помощью аппенд чайлд вставляется в слайдер

    //imageCat.src =
}

//функция PrevSlide -


    //function PrevSlideFunc() {
        //let imageCat = new Image(100, 100);
        //imageCat.src = OurSliderImages[currentPosition -= 1]

    //}

    /*function NextSlideFunc() {

        imageCat.src = OurSliderImages[ currentPosition += 1];
        RenderImage( imageCat );
        console.log( imageCat );
    }

PrevSlide.addEventListener('click', function() {
    PrevSlideFunc();

});
NextSlide.addEventListener('click', function() {
    NextSlideFunc();
});

*/



