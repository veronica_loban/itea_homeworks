
var Train = {
    name: "Train",
    speed:  "100 p/h",
    passangerNumber: 250,

    run: function() {
        console.log(`${this.name} везёт ${this.passangerNumber} пасажиров со скоростью ${this.speed}`);
    },

    stay: function() {
        this.speed = 0;
        console.log(`${this.name} остановился. Скорость ${this.speed} p/h`);
    },

    getPassangers: function( new_passangers ) {
          new_passangers = 30;
          this.passangerNumber += new_passangers;
          console.log( this.passangerNumber )
    }

}

Train.run();
Train.stay();
Train.getPassangers();



/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
