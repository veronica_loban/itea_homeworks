/*
  localStorage
  window.localStorage
*/
  // console.log( window.localStorage );
// Запись в ЛС
// localStorage.setItem('myCat', 'Tom');
// localStorage.setItem('back', 'blue');
// // Чтение с ЛС
// var cat = localStorage.getItem("myCat");
// // Удаление с ЛС
// localStorage.removeItem("myCat");
// console.log( cat );
// Если не найдено, вернет Null
// var background = localStorage.getItem("back");
// console.log( window.localStorage );
/*
let obj = {
  name: 'Vasya',
  learning: true
}
localStorage.setItem('user', JSON.stringify(obj) );
console.log( JSON.parse( localStorage.getItem("user") ) );
// console.log( JSON.parse( localStorage.getItem("JSON") ) );
//
// if( background !== null){
//   document.body.style.backgroundColor = background;
// }
*/

let button = document.getElementById('button');

let  pageWithNewColor = () => {

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random()*(max-min+1)) + min;
    }

    var r = getRandomIntInclusive(0, 256);
    var g = getRandomIntInclusive(0, 256);
    var b = getRandomIntInclusive(0, 256);


    localStorage.setItem('background', 'rgb(' + r + ',' + g + ',' + b + ')');
    document.body.style.backgroundColor = localStorage.getItem('background');


};

let background = localStorage.getItem("background");




button.addEventListener('click', pageWithNewColor );





/*

  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/
